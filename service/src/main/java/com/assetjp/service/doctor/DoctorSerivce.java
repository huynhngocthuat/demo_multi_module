package com.assetjp.service.doctor;

import com.assetjp.dao.doctor.DoctorRepository;
import com.assetjp.model.doctor.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DoctorSerivce {
  @Autowired private DoctorRepository doctorRepository;

  @PostConstruct
  public void initDoctor() {
    doctorRepository.saveAll(
        Stream.of(
                new Doctor(101, "Huynh Ngoc Thuat", "Cardiac"),
                new Doctor(102, "Huong Le Nguyen", "Eye"))
            .collect(Collectors.toList()));
  }

  public List<Doctor> getDoctors() {
    return doctorRepository.findAll();
  }
}
