package com.assetjp.controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.assetjp.*"})
@EntityScan(basePackages = {"com.assetjp.*"})
@EnableJpaRepositories(basePackages = {"com.assetjp.*"})
public class DemoMultiModule {
  public static void main(String[] args) {
    SpringApplication.run(DemoMultiModule.class, args);
  }
}
