package com.assetjp.controller.doctor;

import com.assetjp.model.doctor.Doctor;
import com.assetjp.service.doctor.DoctorSerivce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DoctorController {
  @Autowired private DoctorSerivce doctorSerivce;

  @GetMapping
  public List<Doctor> getDoctors() {
    return doctorSerivce.getDoctors();
  }
}
